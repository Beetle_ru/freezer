﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using IronPython.Hosting;
using Microsoft.Scripting;
using Microsoft.Scripting.Hosting;
using IronPython.Modules;
using System.Windows.Forms;

namespace Freezer {
    public class Processor {
        private readonly string _cfg;
        private readonly string _scriptsDir;
        private readonly string _libPath;
        private readonly List<Node> _nodeList;
        private readonly Dictionary<string, string> _usedScripts;

        private const string NodeName = "node";
        private const string ArgumentsName = "arguments";
        private const string ActionsName = "actions";
        private const string ValueName = "value";
        private const string SrcName = "src";
        private const string ScriptsName = "scripts";
        private const string ScriptName = "script";

        public MemoryStream ScriptStdout;


        public Processor(string cfgPath, string scrtptsDir, string libPath) {
            _cfg = cfgPath;
            _scriptsDir = scrtptsDir;
            _libPath = libPath;

            _nodeList = new List<Node>();
            ScriptStdout = new MemoryStream();
            _usedScripts = new Dictionary<string, string>();
        }

        public void Execute() {
            ReadConfig();
            Run();
        }

        private void ReadConfig() {
            if (!File.Exists(_cfg)) return;

            using (var f = File.OpenRead(_cfg)) {
                using (var reader = XmlReader.Create(f)) {
                    var n = new Node();
                    var isNode = false;
                    var isArgs = false;
                    var isActions = false;
                    var isScripts = false;
                    var scriptName = "";
                    var tagName = "";
                    var parentTagName = "";

                    while (reader.Read()) {
                        switch (reader.NodeType) {
                            case XmlNodeType.Element:
                                parentTagName = tagName;
                                tagName = reader.Name.ToLower();
                                if ((tagName == NodeName) && !isNode) {
                                    n = new Node();
                                    isNode = true;
                                }

                                if ((tagName == ArgumentsName) && isNode && !isArgs) {
                                    isArgs = true;
                                    break;
                                }

                                if ((tagName == ActionsName) && isNode && !isActions) {
                                    isActions = true;
                                    break;
                                }

                                if ((tagName == ScriptsName)) {
                                    isScripts = true;
                                    break;
                                }

                                if (isArgs) n.Arguments.Add(tagName, "");

                                while (reader.MoveToNextAttribute()) {
                                    var atrrName = reader.Name.ToLower();

                                    if (isArgs) {
                                        if (atrrName == ValueName) {
                                            // by value property for support old format
                                            if (!n.Arguments.ContainsKey(tagName))
                                                n.Arguments.Add(tagName, reader.Value);
                                        }
                                    }

                                    if (isActions)
                                        if (atrrName == SrcName)
                                            n.ScriptBidings.Add(reader.Value);
                                                // by value property for support old format

                                    if (isScripts) if (atrrName == SrcName) scriptName = reader.Value;
                                }

                                break;

                            case XmlNodeType.Text:
                                if (isArgs) {
                                    if (n.Arguments.ContainsKey(tagName)) n.Arguments[tagName] = reader.Value;
                                    else n.Arguments.Add(tagName, reader.Value);
                                }
                                if (isActions) n.ScriptBidings.Add(reader.Value);

                                if (isScripts && (scriptName != ""))
                                    if (!_usedScripts.ContainsKey(scriptName))
                                        _usedScripts.Add(scriptName, reader.Value);

                                break;
                            case XmlNodeType.EndElement:
                                tagName = reader.Name.ToLower();

                                if ((tagName == NodeName) && isNode) {
                                    _nodeList.Add(n);
                                    isNode = false;
                                }

                                if ((tagName == ArgumentsName) && isArgs) isArgs = false;
                                if ((tagName == ActionsName) && isActions) isActions = false;
                                if ((tagName == ScriptsName) && isScripts) isScripts = false;

                                tagName = parentTagName;

                                break;
                        }
                    }
                }
                f.Close();
            }
        }

        public void Run() {
            var cmplScripts = new Dictionary<string, CompiledCode>();
            var options = new Dictionary<string, object>();
            options["Debug"] = true;
            var engine = Python.CreateEngine(options);

            engine.Runtime.IO.SetOutput(ScriptStdout, Encoding.UTF8);

            var sp = engine.GetSearchPaths();
            sp.Add(_libPath);
            engine.SetSearchPaths(sp);

            foreach (var node in _nodeList) {
                var scope = engine.CreateScope();
                scope.SetVariable("Arguments", node.Arguments);

                var scriptName = "";
                try {
                    foreach (var sb in node.ScriptBidings) {
                        scriptName = sb;

                        if (!cmplScripts.ContainsKey(sb)) {
                            ScriptSource source;

                            if (_usedScripts.ContainsKey(sb)) {
                                var scriptCode = Base64Decode(_usedScripts[sb]);
                                source = engine.CreateScriptSourceFromString(scriptCode, SourceCodeKind.AutoDetect);
                            }
                            else {
                                var scriptPath = String.Format("{0}\\{1}", _scriptsDir, sb);

                                if (!File.Exists(scriptPath))
                                    throw new Exception(String.Format("The script \"{0}\" not found", scriptPath));

                                source = engine.CreateScriptSourceFromFile(scriptPath);
                            }


                            cmplScripts.Add(sb, source.Compile());
                        }

                        cmplScripts[sb].Execute(scope);
                    }
                }
                catch (Exception e) {
                    var pyexpt = engine.GetService<ExceptionOperations>().FormatException(e);
                    throw new Exception(String.Format("Script \"{0}\" error:\n\"{1}\"\n {2}", scriptName, e.Message, pyexpt));
                }
            }
        }

        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}