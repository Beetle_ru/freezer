﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Freezer {
    internal class Node {
        public Dictionary<string, string> Arguments;
        public List<string> ScriptBidings;

        public Node() {
            Arguments = new Dictionary<string, string>();
            ScriptBidings = new List<string>();
        }
    }
}