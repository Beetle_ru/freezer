﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;


namespace ActionConfigurator {
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private const string NodeTag = "node";
        private const string ActionTag = "action";
        private const string TranslateFld = "Translate";

        private readonly ObservableCollection<String> _actions;
        private readonly TreeViewItem _rootNode;
        private readonly List<NodeGUI> _nodeGUIList;
        private readonly Dictionary<string, string> _translates;

        private string _currentFilename = "config.xml";

        public string Description { get; set; }

        public MainWindow() {
            InitializeComponent();

            _actions = new ObservableCollection<string>();

            _rootNode = new TreeViewItem();
            _rootNode.Header = "Корень";
            //_rootNode.ToolTip = "Корень конфигурации";
            TV_Nodes.Items.Add(_rootNode);

            _nodeGUIList = new List<NodeGUI>();

            _translates = new Dictionary<string, string>();

            Init();
        }

        public void Init() {
            _actions.Clear();
            _rootNode.Items.Clear();
            _nodeGUIList.Clear();
            BTN_Add.IsEnabled = false;
            BTN_Down.IsEnabled = false;
            BTN_Up.IsEnabled = false;
            Description = "";

            LoadTranslates();

            TvNodesSelectedItemChanged(new object(),
                                       new RoutedPropertyChangedEventArgs<object>(new object(), new object()));
        }

        public void LoadTranslates() {
            _translates.Clear();
            var transFiles = Directory.GetFiles(TranslateFld);
            foreach (var transFile in transFiles) {
                var lines = File.ReadAllLines(transFile);

                foreach (var line in lines) {
                    var kv = line.Split(';');
                    if (kv.Count() > 1) {
                        if (!_translates.ContainsKey(kv[0])) {
                            _translates.Add(kv[0], ApplyFormat(kv[1]));
                        }
                    }
                }
            }
        }

        public string ApplyFormat(string str) {
            return str
                .Replace(@"\'", "\'")
                .Replace(@"\""", "\"")
                .Replace(@"\\", "\\")
                .Replace(@"\0", "\0")
                .Replace(@"\a", "\a")
                .Replace(@"\b", "\b")
                .Replace(@"\f", "\f")
                .Replace(@"\n", "\n")
                .Replace(@"\r", "\r")
                .Replace(@"\t", "\t")
                .Replace(@"\v", "\v")
                .Replace(@"[semicolon]", ";");
        }

        private void BtnAddClick(object sender, RoutedEventArgs e) {
            if ((TV_Nodes.SelectedItem == null) || (TV_Nodes.SelectedItem == _rootNode)) CreateNode();
            else {
                if (TV_Nodes.SelectedItem is TreeViewItem) {
                    var item = TV_Nodes.SelectedItem as TreeViewItem;
                    var selector = new ScriptSelector();
                    selector.ShowDialog();

                    var scriptName = selector.ScriptPath;

                    if (selector.IsSelected) CreateAction(scriptName, item);
                }
            }
        }

        public TreeViewItem CreateNode() {
            var item = new TreeViewItem();
            item.Header = "узел";
            item.Tag = NodeTag;
            _rootNode.Items.Add(item);
            _rootNode.IsExpanded = true;

            _nodeGUIList.Add(new NodeGUI(item));

            return item;
        }

        public void CreateAction(string name, TreeViewItem parent, bool isImportArguments = true) {
            var item = new TreeViewItem();
            item.Header = name;
            item.Tag = ActionTag;
            item.ToolTip = GetTranslate(name);
            parent.IsExpanded = true;
            parent.Items.Add(item);
            if (isImportArguments) ImportArguments(name, parent);
        }

        private void ImportArguments(string scriptName, TreeViewItem node) {
            var args = GetArguments(scriptName);

            foreach (var arg in args) AddArgument(arg, "", node, false);
        }

        private List<string> GetArguments(string scriptName) {
            const string pattern = @"Arguments\s*\[\s*""(.+)""\s*]";

            var res = new List<string>();
            var script = LoadScript(scriptName);
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = rgx.Matches(script);

            if (matches.Count > 0) {
                foreach (Match match in matches) {
                    if (match.Groups.Count >= 2) {
                        var argument = match.Groups[1].Value;
                        res.Add(argument);
                    }
                }
            }
            return res;
        }

        private void AddArgument(string argument, string value, TreeViewItem node, bool isApdateValue = true) {
            var nodeGUI = GetNodeGUI(node);
            if (nodeGUI != null)
                nodeGUI.SetArgument(argument, value, isApdateValue);
        }

        private NodeGUI GetNodeGUI(TreeViewItem node) {
            return _nodeGUIList.FirstOrDefault(nodeGUI => nodeGUI.Query(node));
        }

        private void TvNodesSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e) {
            var item = TV_Nodes.SelectedItem as TreeViewItem;
            var verify = ((item == null) || ((item.Tag as string) == ActionTag));
            BTN_Add.IsEnabled = !verify;
            BTN_Down.IsEnabled = !verify;
            BTN_Up.IsEnabled = !verify;

            if (item != null) {
                BTN_Down.IsEnabled = true;
                BTN_Up.IsEnabled = true;

                DP_Arguments.Children.Clear();
                if (((item.Tag as string) == NodeTag) && (item != _rootNode)) {
                    var dg = new DataGrid();
                    var nodeGUI = GetNodeGUI(item);
                    dg.ItemsSource = nodeGUI.Arguments;
                    dg.IsReadOnly = false;
                    dg.SelectionMode = DataGridSelectionMode.Single;
                    dg.AutoGeneratingColumn += dg_AutoGeneratingColumn;
                    dg.MouseDoubleClick += new MouseButtonEventHandler(dg_MouseDoubleClick);
                    dg.LoadingRow += new EventHandler<DataGridRowEventArgs>(DgLoadingRow);
                    DP_Arguments.Children.Add(dg);

                    LBL_Contant.Content = "Параметры узла:";
                }
                else if ((item.Tag as string) == ActionTag) {
                    var tb = new TextBox();
                    tb.TextWrapping = TextWrapping.Wrap;
                    tb.AcceptsReturn = true;
                    tb.IsReadOnly = false;
                    tb.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                    tb.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                    tb.Text = LoadScript(item.Header as string);
                    DP_Arguments.Children.Add(tb);

                    LBL_Contant.Content = "Скрипт(действие):";
                }
                else if (item == _rootNode) { // TODO description configuration
                    var tb = new TextBox();
                    tb.TextWrapping = TextWrapping.Wrap;
                    tb.AcceptsReturn = true;
                    tb.IsReadOnly = false;
                    tb.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                    tb.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;

                    var bnd = new Binding("Description");
                    bnd.Source = this;
                    tb.SetBinding(TextBox.TextProperty, bnd);

                    DP_Arguments.Children.Add(tb);

                    LBL_Contant.Content = "Описание конфигурации:";

                    // for display xml tree
                    //var cb = new ConfigBuilder(_nodeGUIList, ScriptSelector.ScriptDirectory);
                    //cb.Build();

                    //tb.Text = cb.Xml;
                    //DP_Arguments.Children.Add(tb);

                    //BTN_Down.IsEnabled = false;
                    //BTN_Up.IsEnabled = false;

                    //LBL_Contant.Content = "Конфигурация проекта:";
                }
            }
        }

        void DgLoadingRow(object sender, DataGridRowEventArgs e) {
            var index = e.Row.GetIndex();
            var argument = e.Row.DataContext as ActionConfigurator.Argument;

            if (argument == null) return;

            e.Row.ToolTip = GetTranslate(argument.ArgumentName);
        }

        public string GetTranslate(string str) {
            if (_translates.ContainsKey(str)) {
                return _translates[str];
            }

            return null;
        }

        void dg_MouseDoubleClick(object sender, MouseButtonEventArgs e) {
            const string fileSaveExt = "fs";
            const string fileOpenExt = "fo";
            const string fileMultiOpenExt = "fmo";
            const string directorySelectExt = "ds";

            var dg = sender as DataGrid;
            if (dg == null) return;

            var arg = dg.SelectedItem as Argument;
            if (arg == null) return;

            var splt = arg.ArgumentName.Split('.');
            if (splt.Count() < 2) return;

            var ext = splt.Last().ToLower();

            if (ext == fileSaveExt) {
                var dialog = new SaveFileDialog();

                if (dialog.ShowDialog() == true) {
                    arg.ArgumentValue = dialog.FileName;
                }
                
            } else if (ext == fileOpenExt) {

                var fiDlg = new OpenFileDialog();
                fiDlg.Multiselect = false;
                fiDlg.ReadOnlyChecked = true;
                fiDlg.RestoreDirectory = false;
                fiDlg.ValidateNames = false;

                if (fiDlg.ShowDialog() == true) {
                    arg.ArgumentValue = fiDlg.FileName;
                }

            }
            else if (ext == fileMultiOpenExt)
            {
                if (arg.ArgumentValue != "") {
                    if (
                        MessageBox.Show("Очистить список файлов?", "Подтверждение очистки списка файлов",
                                        MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes) {
                        arg.ArgumentValue = "";
                    }
                }

                var fiDlg = new OpenFileDialog();
                fiDlg.Multiselect = true;
                fiDlg.ReadOnlyChecked = true;
                fiDlg.RestoreDirectory = false;
                fiDlg.ValidateNames = false;

                if (fiDlg.ShowDialog() == true)
                {
                    foreach (var fileName in fiDlg.FileNames)
                    {
                        if (arg.ArgumentValue != "") arg.ArgumentValue += ";";
                        arg.ArgumentValue += fileName;
                    }
                }

            } else if (ext == directorySelectExt)
            {

                var foDlg = new System.Windows.Forms.FolderBrowserDialog();

                if (foDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    arg.ArgumentValue = foDlg.SelectedPath;
                }
            }

        }

        private void dg_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e) {
            var t = typeof (Argument);
            var lastPropName = t.GetProperties().Last().Name;

            if (e.Column.Header.ToString() == lastPropName) {
                e.Column.IsReadOnly = false;
                e.Column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else e.Column.IsReadOnly = true;
        }

        private string LoadScript(string filename) {
            var str = "";
            try {
                str = File.ReadAllText(String.Format("{0}\\{1}", ScriptSelector.ScriptDirectory, filename));
            }
            catch (Exception e) {
                MessageBox.Show(e.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return str;
        }

        private void BtnSaveClick(object sender, RoutedEventArgs e) {
            var cb = new ConfigBuilder(_nodeGUIList, ScriptSelector.ScriptDirectory);
            cb.Description = Base64Encode(Description);
            cb.Build();

            var dialog = new SaveFileDialog();
            dialog.FileName = _currentFilename.Split('\\').Last();
            dialog.Filter = "Packet Files(*.xml)|*.xml|All(*.*)|*";

            if (dialog.ShowDialog() == true) cb.Save(dialog.FileName);
        }

        private void BtnNewClick(object sender, RoutedEventArgs e) {
            Init();
        }

        private void BtnOpenClick(object sender, RoutedEventArgs e) {
            var fiDlg = new OpenFileDialog();
            fiDlg.Multiselect = false;
            fiDlg.ReadOnlyChecked = true;
            fiDlg.RestoreDirectory = false;
            fiDlg.ValidateNames = false;

            if (fiDlg.ShowDialog() == true) {
                Init();
                try {
                    var loader = new ConfigLoader(CreateNode, CreateAction, AddArgument);
                    _currentFilename = fiDlg.FileName;
                    loader.Load(_currentFilename);
                    TvNodesSelectedItemChanged(new object(),
                                               new RoutedPropertyChangedEventArgs<object>(new object(), new object()));

                    if (!String.IsNullOrEmpty(loader.Description)) Description = Base64Decode(loader.Description);
                } 
                catch(Exception ex) {
                    var msg = String.Format("Не корректный файл конфигурации вызвал прерывание: \n{0}", ex.Message);
                    MessageBox.Show(msg, "Ошибка открытия", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void BtnRemooveClick(object sender, RoutedEventArgs e) {
            var item = TV_Nodes.SelectedItem as TreeViewItem;

            if ((item == null) || (item == _rootNode)) return;

            var tag = item.Tag as string;
            if (tag == NodeTag) {
                var node = GetNodeGUI(item);

                _nodeGUIList.Remove(node);
                _rootNode.Items.Remove(item);
            }
            else if (tag == ActionTag) {
                var parent = item.Parent as TreeViewItem;
                if (parent == null) return;

                var node = GetNodeGUI(parent);
                parent.Items.Remove(item);

                var remooveActionName = item.Header as string;
                var remooveActionArguments = GetArguments(remooveActionName);

                var actualArguments = new List<string>();

                foreach (var itm in parent.Items) {
                    // precompile arguments without remooved action
                    var treeViewItem = itm as TreeViewItem;
                    if (treeViewItem == null) continue;
                    var aa = GetArguments(treeViewItem.Header as string);

                    foreach (var a in aa) if (!actualArguments.Contains(a)) actualArguments.Add(a);
                }

                foreach (var a in remooveActionArguments) {
                    if (!actualArguments.Contains(a)) {
                        for (var i = 0; i < node.Arguments.Count; i++) {
                            if (node.Arguments[i].ArgumentName == a) {
                                node.Arguments.RemoveAt(i); // remoove non actual arguments
                                break;
                            }
                        }
                    }
                }
            }
        }

        private void BtnUpClick(object sender, RoutedEventArgs e) {
            var item = TV_Nodes.SelectedItem as TreeViewItem;
            if ((item == null) || (item == _rootNode)) return;

            MooveItemView(item, -1);
        }

        private void BtnDownClick(object sender, RoutedEventArgs e) {
            var item = TV_Nodes.SelectedItem as TreeViewItem;
            if ((item == null) || (item == _rootNode)) return;

            MooveItemView(item, 1);
        }

        private void MooveItemView(TreeViewItem item, int shift) {
            var parent = item.Parent as TreeViewItem;
            Debug.Assert(parent != null, "MooveItem: parent != null");

            var index = parent.Items.IndexOf(item);
            var parentCnt = parent.Items.Count;

            if ((index < 0) || (index >= parentCnt)) return;

            var newIndex = index + shift;
            newIndex = newIndex < 0 ? 0 : newIndex;
            newIndex = newIndex >= parentCnt - 1 ? parentCnt - 1 : newIndex;

            if (index == newIndex) return;

            parent.Items.RemoveAt(index);
            parent.Items.Insert(newIndex, item);

            item.IsSelected = true;

            MooveNodeItem(item, shift);
        }

        private void MooveNodeItem(TreeViewItem item, int shift) {
            if ((item.Tag as string) == NodeTag) {
                var parent = _nodeGUIList;
                var itm = GetNodeGUI(item);
                var index = parent.IndexOf(itm);
                var parentCnt = parent.Count;
                if ((index < 0) || (index >= parentCnt)) return;
                var newIndex = index + shift;
                newIndex = newIndex < 0 ? 0 : newIndex;
                newIndex = newIndex >= parentCnt - 1 ? parentCnt - 1 : newIndex;
                if (index == newIndex) return;
                parent.RemoveAt(index);
                parent.Insert(newIndex, itm);
            }
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}