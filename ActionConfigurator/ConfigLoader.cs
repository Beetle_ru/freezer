﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Xml;

namespace ActionConfigurator {
    internal delegate TreeViewItem CreateNodeDelegate();

    internal delegate void CreateActionDelegate(string name, TreeViewItem parent, bool isImportArguments = true);

    internal delegate void AddArgumentDelegate(
        string argument, string value, TreeViewItem node, bool isApdateValue = true);

    internal class ConfigLoader {
        private CreateNodeDelegate _createNode;
        private CreateActionDelegate _createAction;
        private AddArgumentDelegate _addArgument;

        private const string NodeName = "node";
        private const string ArgumentsName = "arguments";
        private const string ActionsName = "actions";
        private const string ValueName = "value";
        private const string SrcName = "src";
        private const string ScriptsName = "scripts";
        private const string ScriptName = "script";
        private const string ConfigDescription = "description";

        public string Description;

        public ConfigLoader(CreateNodeDelegate createNode, CreateActionDelegate createAction,
                            AddArgumentDelegate addArgument) {
            _createNode = createNode;
            _createAction = createAction;
            _addArgument = addArgument;
        }

        public bool Load(string configFile) {
            if (!File.Exists(configFile)) return false;

            using (var f = File.OpenRead(configFile)) {
                using (var reader = XmlReader.Create(f)) {
                    var tagName = "";
                    var parentTagName = "";
                    var curentNode = new TreeViewItem();
                    var isActions = false;
                    var isArgs = false;
                    var isArgumentAdded = false;
                    var isDescription = false;

                    while (reader.Read()) {
                        switch (reader.NodeType) {
                            case XmlNodeType.Element:
                                parentTagName = tagName;
                                tagName = reader.Name.ToLower();

                                if ((tagName == NodeName)) curentNode = _createNode();

                                if ((tagName == ArgumentsName) && !isArgs) {
                                    isArgs = true;
                                    break;
                                }

                                if ((tagName == ConfigDescription) && !isDescription) {
                                    isDescription = true;
                                    break;
                                }

                                if ((tagName == ActionsName) && !isActions) {
                                    isActions = true;
                                    break;
                                }

                                if (isArgs) isArgumentAdded = false;

                                break;
                            case XmlNodeType.Text:
                                if (isArgs) {
                                    _addArgument(tagName, reader.Value, curentNode);
                                    isArgumentAdded = true;
                                }
                                if (isActions) _createAction(reader.Value, curentNode, false);

                                if (isDescription) Description = reader.Value;

                                break;
                            case XmlNodeType.EndElement:
                                tagName = reader.Name.ToLower();

                                if ((tagName == ActionsName) && isActions) isActions = false;
                                if ((tagName == ArgumentsName) && isArgs) isArgs = false;
                                if ((tagName == ConfigDescription) && isDescription) isDescription = false;

                                if (!isArgumentAdded) _addArgument(tagName, "", curentNode);

                                tagName = parentTagName;

                                break;
                        }
                    }
                }
                f.Close();
            }
            return false;
        }
    }
}