﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Collections.ObjectModel;

namespace ActionConfigurator {
    internal class NodeGUI {
        public ObservableCollection<Argument> Arguments;
        public TreeViewItem NodeViewItem;

        public NodeGUI(TreeViewItem nodeViewItem) {
            Arguments = new ObservableCollection<Argument>();
            NodeViewItem = nodeViewItem;
        }

        public bool Query(TreeViewItem item) {
            return item == NodeViewItem;
        }

        public List<string> GetActions() {
            var items = NodeViewItem.Items;
            return (from object item in items select ((TreeViewItem) item).Header as string).ToList();
        }

        public void SetArgument(string key, string value, bool isApdate = true) {
            foreach (var argument in Arguments) {
                if (argument.ArgumentName == key) {
                    if (isApdate) argument.ArgumentValue = value;
                    return;
                }
            }

            Arguments.Add(new Argument(key, value));
        }
    }

    internal class Argument : INotifyPropertyChanged {
        private string _argumentName;
        private string _argumentValue;

        public string ArgumentName {
            get { return _argumentName; }
            set {
                _argumentName = value;
                NotifyPropertyChanged();
            }
        }

        public string ArgumentValue {
            get { return _argumentValue; }
            set {
                _argumentValue = value;
                NotifyPropertyChanged();
            }
        }

        public Argument(string name, string value) {
            ArgumentName = name;
            ArgumentValue = value;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String propertyName = "") {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}