﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ActionConfigurator {
    /// <summary>
    /// Логика взаимодействия для ScriptSelector.xaml
    /// </summary>
    public partial class ScriptSelector : Window {
        public const string ScriptDirectory = "Scripts";

        public bool IsSelected;
        public string ScriptPath;

        private readonly ObservableCollection<String> _scripts;

        public ScriptSelector() {
            InitializeComponent();

            BTN_Add.IsEnabled = false;

            _scripts = new ObservableCollection<string>();
            LB_Scripts.ItemsSource = _scripts;

            SearchScripts();
        }

        private void BtnAddClick(object sender, RoutedEventArgs e) {
            AddScript();

            Close();
        }

        private void AddScript() {
            var index = LB_Scripts.SelectedIndex;

            if (index < 0) return;

            ScriptPath = _scripts[index];
            IsSelected = true;
        }

        private void BtnCancelClick(object sender, RoutedEventArgs e) {
            IsSelected = false;
            ScriptPath = "";
            Close();
        }

        public List<string> GetFilesRecursive(string directory, string pattern) {
            var files = Directory.GetFiles(directory, pattern).ToList();
            var dirs = Directory.GetDirectories(directory);
            files.AddRange(dirs.SelectMany(dir => GetFilesRecursive(dir, pattern)));
            return files;
        }

        public void SearchScripts() {
            var files = GetFilesRecursive(ScriptDirectory, "*.py");
            foreach (var file in files)
                _scripts.Add(file.Split('\\').Last());
        }

        private void LbScriptsSelectionChanged(object sender, SelectionChangedEventArgs e) {
            if (LB_Scripts.SelectedIndex >= 0) BTN_Add.IsEnabled = true;
        }

        private void LbScriptsMouseDoubleClick(object sender, MouseButtonEventArgs e) {
            AddScript();

            Close();
        }
    }
}