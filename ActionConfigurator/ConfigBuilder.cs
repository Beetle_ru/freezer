﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.IO;

namespace ActionConfigurator {
    internal class ConfigBuilder {
        public List<NodeGUI> Nodes;
        public string Xml;
        public string ScriptDir;
        public string Description;

        public ConfigBuilder(List<NodeGUI> nodes, string scriptDirectory = "") {
            Nodes = nodes;
            ScriptDir = scriptDirectory;
        }

        public void Build() {
            var output = new StringBuilder();
            var ws = new XmlWriterSettings();
            ws.Indent = true;
            using (var writer = XmlWriter.Create(output, ws)) {
                writer.WriteProcessingInstruction("xml", "version = '1.0'");

                writer.WriteStartElement("configuration");

                writer.WriteStartElement("description");
                writer.WriteString(Description);
                writer.WriteFullEndElement(); // description

                foreach (var nodeGUI in Nodes) {
                    writer.WriteStartElement("node");

                    var arguments = nodeGUI.Arguments;
                    var actions = nodeGUI.GetActions();

                    writer.WriteStartElement("arguments");
                    foreach (var argument in arguments) {
                        writer.WriteStartElement(argument.ArgumentName);
                        writer.WriteString(argument.ArgumentValue);
                        writer.WriteFullEndElement();
                    }
                    writer.WriteFullEndElement(); // arguments

                    writer.WriteStartElement("actions");
                    foreach (var action in actions) {
                        writer.WriteStartElement("action");
                        writer.WriteString(action);
                        writer.WriteFullEndElement(); // action
                    }
                    writer.WriteFullEndElement(); // actions


                    writer.WriteFullEndElement(); // node
                }

                WriteScriptSection(writer);

                writer.WriteFullEndElement(); // configuration
            }

            Xml = output.ToString();
        }

        private void WriteScriptSection(XmlWriter writer) {
            if (ScriptDir == "") return;

            var usedScripts = new Dictionary<string, string>();

            foreach (var nodeGUI in Nodes) {
                foreach (var action in nodeGUI.GetActions()) {
                    if (usedScripts.ContainsKey(action)) continue;

                    var contant = GetScriptBase(action);
                    usedScripts.Add(action, Base64Encode(contant));
                }
            }

            writer.WriteStartElement("scripts");

            foreach (var usedScript in usedScripts) {
                writer.WriteStartElement("script");
                writer.WriteAttributeString("src", usedScript.Key);
                writer.WriteString(usedScript.Value);
                writer.WriteFullEndElement(); // script
            }

            writer.WriteFullEndElement(); // scripts
        }

        public static string Base64Encode(string plainText) {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        private string GetScriptBase(string scriptName) {
            var res = "";
            var path = String.Format("{0}\\{1}", ScriptDir, scriptName);
            try {
                res = File.ReadAllText(path);
            }
            catch (Exception e) {
                MessageBox.Show(e.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return res;
        }

        public void Save(string fileName) {
            try {
                File.WriteAllText(fileName, Xml);
            }
            catch (Exception e) {
                MessageBox.Show(e.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}