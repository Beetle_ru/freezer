import zipfile

# in vars
dstDir = Arguments["unzipdir_destinationdir.ds"]
zipfileName = Arguments["unzipdir_zipfile.fo"]

print '*** unzipping file "' + zipfileName + '" to "' + dstDir + '" ***'

fh = open(zipfileName, 'rb')
z = zipfile.ZipFile(fh)
for name in z.namelist():
    z.extract(name, dstDir)
    filename = name.split("/")[-1]
    if filename:
        print "*** UnZip > " + filename
fh.close()