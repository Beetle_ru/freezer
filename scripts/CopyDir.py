import os
import re
import shutil

# in vars
src = Arguments["cpdir_source.ds"]
dst = Arguments["cpdir_destination.ds"]
regExpListStr = Arguments["cpdir_explst"]

# compile regular expressions
regExpArr = regExpListStr.split(";")
compiledExpList = []
if len(regExpListStr):
    for regExpItem in regExpArr:
        p = re.compile(regExpItem)
        compiledExpList.append(p)

# get regular files and subdirs
for root, dirs, files in os.walk(src):
    for name in files:
        new_path = root.replace(src, dst, 1)
        fullname = os.path.join(root, name)
        newFullname = os.path.join(new_path, name)

        # verify file names
        isIgnore = False
        for p in compiledExpList:
            if p.search(name):
                isIgnore = True
                break

        # copying
        if not isIgnore:
            try:
                if not os.path.exists(new_path):
                    os.makedirs(new_path)
                    print "*** Create > " + new_path
                print "*** Copy > " + name
                shutil.copyfile(fullname, newFullname)
            except Exception, e:
                print "##### ERROR #####"
                print e