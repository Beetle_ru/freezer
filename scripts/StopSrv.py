# !EXPORT stopsrv_service

import subprocess

# in vars
srv = Arguments["stopsrv_service"]

cmd = []
cmd.append("sc")
cmd.append("stop")
cmd.append(srv)

p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

isWrited = False
while p.poll() is None:
    stdout = p.stdout.read()
    print stdout.decode('cp866')
    isWrited = True

p.wait()

if not isWrited:
    stdout = p.stdout.read()
    print stdout.decode('cp866')