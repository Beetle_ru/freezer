import subprocess

# in vars
filename = Arguments["unzipdir_zipfile.fo"]

cmd = []
cmd.append("tools\FileOpenDlg_cli")
cmd.append(filename)
cmd.append("Packet Files(*.zip)|*.zip|All(*.*)|*")

p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
p.wait()

stdout = p.stdout.read()
stdout = stdout.decode('cp866')

filename = [stdout, filename][stdout == ""]

# out vars
Arguments["unzipdir_zipfile.fo"] = filename