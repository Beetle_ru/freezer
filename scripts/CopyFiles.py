import os
import re
import shutil

# in vars
srcList = Arguments["cpfiles_files.fmo"]
srcDir = Arguments["cpfiles_srcdir.ds"]
dstDir = Arguments["cpfiles_destination.ds"]

if len(srcList):
    splt = srcList.split(";")

    for sourceFilePath in splt:
        newFilePath = sourceFilePath.replace(srcDir, dstDir, 1)

        try:
            fileFolder = os.path.dirname(newFilePath)
            if not os.path.exists(fileFolder):
                os.makedirs(fileFolder)
                print "*** Create > " + fileFolder
            print "*** Copy > " + sourceFilePath
            shutil.copyfile(sourceFilePath, newFilePath)
        except Exception, e:
            print "##### ERROR #####"
            print e

