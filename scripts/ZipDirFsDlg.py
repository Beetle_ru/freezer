import subprocess

# in vars
filename = Arguments["zipdir_zipname.fs"]

cmd = []
cmd.append("tools\FileSaveDlg_cli")
cmd.append(filename)
cmd.append("Packet Files(*.zip)|*.zip|All(*.*)|*")

p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
p.wait()

stdout = p.stdout.read()
stdout = stdout.decode('cp866')

filename = [stdout, filename][stdout == ""]

# out vars
Arguments["zipdir_zipname.fs"] = filename