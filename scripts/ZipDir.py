import os
import zipfile

# in vars
srcDir = Arguments["zipdir_sourcedir.ds"]
zipName = Arguments["zipdir_zipname.fs"]

print '*** zipping directory "' + srcDir + '" to "' + zipName + '" ***'

zf = zipfile.ZipFile(zipName, "w")
for dirname, subdirs, files in os.walk(srcDir):
    dstDirname = dirname.replace(srcDir, '', 1)
    if dstDirname != '':
        if dstDirname[0] == '\\':
            dstDirname = dstDirname[1:]
        zf.write(dirname, dstDirname)

    for filename in files:
        srcFile = os.path.join(dirname, filename)
        dstFile = os.path.join(dstDirname, filename)
        zf.write(srcFile, dstFile)
        print "*** Zip > " + filename

zf.close()
