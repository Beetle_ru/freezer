import os
import re
import shutil

# in vars
src = Arguments["cpsinglefile_src.fo"]
dst = Arguments["cpsinglefile_dst.fs"]


try:
    fileFolder = os.path.dirname(dst)
    if not os.path.exists(fileFolder):
        os.makedirs(fileFolder)
        print "*** Create > " + fileFolder
    print "*** Copy > " + src
    shutil.copyfile(src, dst)
except Exception, e:
    print "##### ERROR #####"
    print e
