import os
import shutil

# in vars
dir = Arguments["rmdir_directory.ds"]

if os.path.exists(dir):
    shutil.rmtree(dir)