﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FileSaveDlg_cli {
    public partial class Runner : Form {
        public Runner() {
            InitializeComponent();
            this.Hide();
        }

        private void RunnerLoad(object sender, EventArgs e) {
            this.Hide();
            var dialog = new SaveFileDialog();
            if (Program.ARGS.Any()) {
                dialog.FileName = Program.ARGS[0];
                if (Program.ARGS.Count() >= 2) dialog.Filter = Program.ARGS[1];
            }
            if (dialog.ShowDialog() == DialogResult.OK)
                Program.RESULT = dialog.FileName;
            this.Close();
        }
    }
}