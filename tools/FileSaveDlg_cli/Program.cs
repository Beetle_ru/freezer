﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FileSaveDlg_cli {
    internal static class Program {
        public static string[] ARGS;
        public static string RESULT;

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        private static void Main(string[] args) {
            ARGS = args;
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Runner());
            Console.Write(RESULT);
        }
    }
}