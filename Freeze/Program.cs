﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Freezer;
using System.IO;
using System.Threading;

namespace Freeze {
    internal class Program {
        private static MemoryStream _stdOutStream;
        private static bool _isNotEmptyStdout;

        private static void Main(string[] args) {
            if (!args.Any()) {
                Console.WriteLine("Freeze cfgName.xml");
                return;
            }

            var prcssr = new Processor(args[0], "./Override/", "Lib");

            _stdOutStream = prcssr.ScriptStdout;

            var printThread = new Thread(Print);
            printThread.IsBackground = true;
            printThread.Start();

            try {
                prcssr.Execute();

                while (_isNotEmptyStdout) {
                    _isNotEmptyStdout = false;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        private static void Print() {
            var offset = 0;
            var buffer = new byte[255];
            var bffrmax = buffer.Count() - 1;
            while (true) {
                var cnt = (int) (_stdOutStream.Length < offset + bffrmax ? _stdOutStream.Length - offset - 1 : bffrmax);
                cnt = cnt > 0 ? cnt : 0;
                if (cnt > 0) {
                    _isNotEmptyStdout = true;
                    var bff = _stdOutStream.GetBuffer();
                    for (int i = offset; i < offset + cnt; i++) buffer[i - offset] = bff[i];
                    offset += cnt;

                    var str = Encoding.UTF8.GetString(buffer).Trim('\0', '\n', '\r');
                    if (str != "") Console.WriteLine(str);

                    for (int i = 0; i < cnt; i++)
                        buffer[i] = 0;
                }

                Thread.Sleep(100);
            }
        }
    }
}