﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml;
using Freezer;
using Microsoft.Win32;
using ThreadState = System.Threading.ThreadState;

namespace AlphaExecutor {
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {
        private const string ConfigsDirectoty = "Configs";

        private readonly ObservableCollection<String> _configs;
        private Thread _executorThread;

        private MemoryStream _stdOutStream;
        private bool _isNotEmptyStdout;

        public MainWindow() {
            Admin();
            InitializeComponent();

            _configs = new ObservableCollection<string>();
            CB_Configs.ItemsSource = _configs;

            var printThread = new Thread(Print);
            printThread.IsBackground = true;
            printThread.Start();

            Init();
        }

        private void Init() {
            RefrashConfigs();
            CbConfigsSelectionChanged(null, null);
        }

        public void RefrashConfigs() {
            var cfgs = Directory.GetFiles(ConfigsDirectoty, "*.xml");

            _configs.Clear();
            foreach (var cfg in cfgs) _configs.Add(cfg.Split('\\').Last());
        }

        private void Execute() {
            var cfg = "";
            Dispatcher.Invoke(new Action(() => cfg = CB_Configs.SelectedValue as string));

            if (string.IsNullOrEmpty(cfg)) return;

            cfg = String.Format("{0}\\{1}", ConfigsDirectoty, cfg);

            var prcssr = new Processor(cfg, "./Override/", "Lib");

            _stdOutStream = prcssr.ScriptStdout;

            try {
                prcssr.Execute();

                while (_isNotEmptyStdout) {
                    _isNotEmptyStdout = false;
                    Thread.Sleep(1000);
                }
            }
            catch (Exception e) {
                this.Dispatcher.Invoke(new Action(() => this.ConsolePush(e.Message)));
            }
        }

        private void Print() {
            var offset = 0;
            var buffer = new byte[255];
            var bffrmax = buffer.Count() - 1;
            var isStoped = false;
            var animIndex = 0;

            var anims = new List<string[]>();
            var anim = new string[] {"←", "↖", "↑", "↗", "→", "↘", "↓", "↙"};
            anims.Add(anim);

            anim = new string[] {
                                        "       ", 
                                        ">      ", 
                                        "=>     ", 
                                        "~=>    ", 
                                        " ~=>   ", 
                                        "  ~=>  ", 
                                        "   ~=> ", 
                                        "    ~=>", 
                                        "     ~=", 
                                        "      ~", 
                                        "       ", 
                                        "      <", 
                                        "     <=", 
                                        "    <=~", 
                                        "   <=~ ", 
                                        "  <=~  ", 
                                        " <=~   ", 
                                        "<=~    ", 
                                        "=~     ", 
                                        "~      "
                                    };
            anims.Add(anim);

            anim = new string[] {
                                        "       ",
                                        ". . . .",
                                        "- - - -",
                                        "+ + + +",
                                        "@ @ @ @",
                                        "+ + + +",
                                        "- - - -",
                                        ". . . .",
                                        "       ",
                                        " . . . ",
                                        " - - - ",
                                        " + + + ",
                                        " @ @ @ ",
                                        " + + + ",
                                        " - - - ",
                                        " . . . "
                                    };
            anims.Add(anim);
            
            while (true) {
                if (_executorThread != null) {
                    var state = _executorThread.ThreadState;

                    if (!isStoped && (state == ThreadState.Aborted) || (state == ThreadState.Stopped)) {
                        Dispatcher.Invoke(new Action(() => BTN_Execute.Content = "Выполнить"));
                        Dispatcher.Invoke(new Action(() => BTN_Execute.IsEnabled = true));
                        offset = 0;
                        _stdOutStream = new MemoryStream();

                        isStoped = true;

                        var r = new Random();
                        anim = anims[r.Next(anims.Count)];
                    }
                    else {
                        isStoped = false;

                        if (animIndex >= anim.Count()) animIndex = 0;
                        var s = anim[animIndex];
                        Dispatcher.Invoke(new Action(() => BTN_Execute.Content = s));
                        animIndex++;

                        Dispatcher.Invoke(new Action(() => BTN_Execute.IsEnabled = false));

                        var cnt =
                            (int)
                            (_stdOutStream.Length < offset + bffrmax ? _stdOutStream.Length - offset - 1 : bffrmax);
                        cnt = cnt > 0 ? cnt : 0;
                        if (cnt > 0) {
                            _isNotEmptyStdout = true;
                            var bff = _stdOutStream.GetBuffer();
                            for (int i = offset; i < offset + cnt; i++) buffer[i - offset] = bff[i];
                            offset += cnt;

                            var str = Encoding.UTF8.GetString(buffer).Trim('\0', '\n', '\r');
                            if (str != "") this.Dispatcher.Invoke(new Action(() => this.ConsolePush(str)));

                            for (int i = 0; i < cnt; i++)
                                buffer[i] = 0;
                        }
                    }

                    Thread.Sleep(100);
                }
                else {
                    Thread.Sleep(300);
                    continue;
                }
            }
        }

        public void ConsolePush(string message) {
            TB_Log.AppendText("*" + message + "\n");
            TB_Log.ScrollToVerticalOffset(TB_Log.VerticalOffset + Double.MaxValue);
        }

        private void BtnExecuteClick(object sender, RoutedEventArgs e) {
            if ((_executorThread == null) || (_executorThread.ThreadState == ThreadState.Aborted) ||
                (_executorThread.ThreadState == ThreadState.Stopped)) {
                _executorThread = new Thread(Execute);
                _executorThread.IsBackground = true;
                _executorThread.Start();
            }
            else {
                _executorThread.Abort();
                _executorThread.Join();
            }
        }

        private void BtnAddClick(object sender, RoutedEventArgs e) {
            var fiDlg = new OpenFileDialog();
            fiDlg.Multiselect = false;
            fiDlg.ReadOnlyChecked = true;
            fiDlg.RestoreDirectory = false;
            fiDlg.ValidateNames = false;
            if (fiDlg.ShowDialog() == true) {
                var src = fiDlg.FileName;
                var dst = String.Format("{0}\\{1}", ConfigsDirectoty, src.Split('\\').Last());

                if (File.Exists(dst)) {
                    var res = MessageBox.Show("Файл с таким именем уже существует! Перезаписать?", "Предупрждение!!!",
                                              MessageBoxButton.YesNo, MessageBoxImage.Question);

                    if (res == MessageBoxResult.No) return;

                    File.Delete(dst);
                }

                File.Copy(src, dst);
                RefrashConfigs();
            }
        }

        private void BtnRmClick(object sender, RoutedEventArgs e) {
            var currentScript = CB_Configs.SelectedItem as string;

            if (String.IsNullOrWhiteSpace(currentScript)) return;

            var src = String.Format("{0}\\{1}", ConfigsDirectoty, currentScript);

            if (!File.Exists(src)) {
                MessageBox.Show("Что-то пошло не так", "Сбой!!!", MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }

            var msg = String.Format("Файл {0} будет удален! Продолжить?", currentScript);
            var res = MessageBox.Show(msg, "Предупрждение!!!", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (res == MessageBoxResult.No) return;

            try {
                File.Delete(src);
            }
            catch(Exception ex) {
                MessageBox.Show(ex.Message, "Ошибка удаления файла", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            RefrashConfigs();
        }

        private void CbConfigsSelectionChanged(object sender, SelectionChangedEventArgs e) {
            var item = CB_Configs.SelectedItem as string;

            TB_Log.Text = "";

            if (String.IsNullOrWhiteSpace(item)) {
                BTN_Rm.IsEnabled = false;
                BTN_Execute.IsEnabled = false;
            }
            else {
                BTN_Rm.IsEnabled = true;
                BTN_Execute.IsEnabled = true;

                ReadDescription(item);
            }
        }

        private void ReadDescription(string configFile) {
            var src = String.Format("{0}\\{1}", ConfigsDirectoty, configFile);
            if (!File.Exists(src)) return;

            const string configDescription = "description";

            using (var f = File.OpenRead(src)) {
                using (var reader = XmlReader.Create(f)) {
                    var tagName = "";
                    var isDescription = false;

                    while (reader.Read()) {
                        switch (reader.NodeType) {
                            case XmlNodeType.Element:
                                tagName = reader.Name.ToLower();

                                if ((tagName == configDescription) && !isDescription) {
                                    isDescription = true;
                                    break;
                                }


                                break;
                            case XmlNodeType.Text:

                                if (isDescription && (!String.IsNullOrEmpty(reader.Value))) {
                                    TB_Log.Text = Base64Decode(reader.Value);
                                    return;
                                }

                                break;
                            case XmlNodeType.EndElement:
                                tagName = reader.Name.ToLower();

                                if ((tagName == configDescription) && isDescription) isDescription = false;

                                break;
                        }
                    }
                }
                f.Close();
            }
        }

        public static string Base64Decode(string base64EncodedData) {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }





        public static void Admin() {
            var ident = WindowsIdentity.GetCurrent();
            var isAdministrativeRight = false;
            if (ident != null) {
                var pricipal = new WindowsPrincipal(ident);
                isAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            if (!isAdministrativeRight) {
                var processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas"; //указываем, что процесс должен быть запущен с правами администратора
                processInfo.FileName = Environment.GetCommandLineArgs()[0];

                try {
                    Process.Start(processInfo);
                }
                catch (Exception) {
                    //Ничего не делаем, потому что пользователь, возможно, нажал кнопку "Нет" в ответ на вопрос о запуске программы в окне предупреждения UAC (для Windows 7)
                }
                Environment.Exit(0);
            }
        }
    }
}